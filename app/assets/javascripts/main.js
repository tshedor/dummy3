$(document).ready(function(){
  $('h1').text('This text has been changed by JavaScript');
  if(Modernizr.flexbox) {
    $('h1').text('This browser supports flexbox');
  }
});
